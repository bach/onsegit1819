#!/usr/bin/python
# -*- coding: utf-8 -*-

# @Author: Bachir Soussi Chiadmi <bach>
# @Date:   29-11-2018
# @Email:  bachir@esadhar.net
# @Last modified by:   bach
# @Last modified time: 29-11-2018
# @License: GPL-V3


import sys, os, shutil
import markdown
# import mistune
from bs4 import BeautifulSoup
import pypandoc
# import json
import re
import requests
from urllib.parse import urlparse


_SRC_ASSETS_d = "assets"
_BUILD_d = "build"
_ASSETS_d = os.path.join(_BUILD_d, 'assets')
_IMG_d = os.path.join(_ASSETS_d, 'images')
_CSS_d = os.path.join(_ASSETS_d, 'css')
# CUR_PATH = os.path.dirname(os.path.abspath(__file__))

print("Building book")

def main():
   # clean build directory
   if os.path.isdir(_BUILD_d):
      shutil.rmtree(_BUILD_d, ignore_errors=True)
   os.mkdir(_BUILD_d)
   os.mkdir(_ASSETS_d)
   os.mkdir(_CSS_d)
   os.mkdir(_IMG_d)

   shutil.copy(os.path.join(_SRC_ASSETS_d, 'css/dist/main.css'), os.path.join(_CSS_d, 'main.css'))

   parse_pages()



def parse_pages():
   print("Parse book")

   # get template html
   template_f = open("templates/index.tpl.html", "r")
   template_html = template_f.read()
   template_dom = BeautifulSoup(template_html, 'html.parser')

   for n in range(1,25):
      if n < 10 :
         file_num = "0"+str(n)
      else:
         file_num = str(n)

      print(file_num);

      file_path = '../'+file_num+'.md'
      if not os.path.isfile(file_path):
         print("Not a file: "+file_num)
         continue

      # file = open(file_path, "r")
      # text = file.read()
      # print(text)

      text_html = pypandoc.convert_file(file_path, 'html')
      text_dom = BeautifulSoup(text_html, 'html.parser')

      paper_f = open("templates/page.tpl.html", "r")
      paper_html = paper_f.read()
      paper_dom = BeautifulSoup(paper_html, "html.parser")
      paper_dom.find('div', {"class":"paper"})['id'] = "text-"+file_num
      paper_dom.find('div',{"class":"body"}).append(text_dom)
      template_dom.find('div', {"id":"couve3"}).insert_before(paper_dom)

   # TODO: copy images localy
   for img in template_dom.find_all('img'):
      src = img['src']
      print(src)
      a = urlparse(src)
      file_name = os.path.basename(a.path)
      img_path = os.path.join(_IMG_d, file_name)
      try:
         r = requests.get(src)
         with open(img_path, 'wb') as outfile:
            outfile.write(r.content)

         img_src = os.path.join('assets/images', file_name)
         img['src'] = img_src
      except OSError:
         pass


   # Ajouter le readme
   readme_html = pypandoc.convert_file('../README.md', 'html')
   readme_dom = BeautifulSoup(readme_html, 'html.parser')

   # insert a first empty page
   paper_f = open("templates/page.tpl.html", "r")
   paper_html = paper_f.read()
   paper_dom = BeautifulSoup(paper_html, "html.parser")
   template_dom.find('div', {"id":"couve3"}).insert_before(paper_dom)
   # the the real readme
   paper_f = open("templates/page.tpl.html", "r")
   paper_html = paper_f.read()
   paper_dom = BeautifulSoup(paper_html, "html.parser")
   paper_dom.find('div', {"class":"paper"})['id'] = "readme"
   paper_dom.find('div',{"class":"body"}).append(readme_dom)
   template_dom.find('div', {"id":"couve3"}).insert_before(paper_dom)

   # Ajoute le pad

   # create main html file from filled template html dom
   build_html_f = os.path.join(_BUILD_d,'index.html')
   with open(build_html_f, 'w') as fp:
      fp.write(template_dom.prettify())



if __name__ == "__main__":
   main()
