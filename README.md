# OnseGit1819

## Atelier d'écriture git 2018-2019
Adresse du pad : https://annuel.framapad.org/p/onsegit1819

### Le principe général
Chaque utilisateur entre un paragraphe d'un longueur d'environ 10 lignes.

Chacun sabote un texte designé, puis corrige un autre, et ainsi de suite.
> Exemple : un texte sans voyelles / les suivants ajoutent les voyelles au risque de dériver du mot de départ

D'une semaine à l'autre, les utilisateur occupent les fonctions suivantes : saboteur puis correcteur.

En fonction du statut occupé, l'utilisateur appliquera une règle de modification du texte.
Les modifications se font sur les paragraphes suivant celui entré par l'utilisateur.
Ainsi on applique le schéma suivant :
- Paragraphe A - Entré par l'utilisateur 1, saboté par l'utilisateur 3, corrigé par l'utilisateur 2
- Paragraphe B - Entré par l'utilisateur 2, saboté par l'utilisateur 1, corrigé par l'utilisateur 3
- Paragraphe C - Entré par l'utilisateur 3, saboté par l'utilisateur 2, corrigé par l'utilisateur 1

Cet ordre varie en fonction des semaines et du dernier texte modifié.

Peut-on saboter selon plusieurs règles en même temps ?
La suppression ne doit pas dépasser plus de 50% du texte.
La correction peut-elle générer la cohérence des paragraphes entre eux ?

## Règles de sabotage

1. Enlever une voyelle/une consonne.
2. Intervertir des mots.
3. Supprimer la ponctuation.
4. Faire des erreurs grammaticales et orthographiques intentionnelles.
5. Remplacer des lettres par leurs valeur numérique dans l'alphabet.
6. Inverser l'ordre des lettres d'un mot.
7. Rajouter 2 'i' lorsqu'on en voit un.
8. Intervertir l'ordre des phrases.
9. Choisir une voyelle et la supprimer à chaque fois qu'elle apparait dans le paragraphes.
10. Enlever deux lettres par mots.
11. Remplacer les mots par leurs contraires.
12. Supprimer tous les e
13. Inverser les ponctuations
14. Supprimer les pronoms personnels (je, tu, il...).
15. Remplacer dans un mot les lettres suivantes (a devient b)
16. Ajouter des images pour représenter un objet, un concept ou une personne
17. Remplacer une voyelle par une autre
18. Ajouter supprimer des espaces
19. Doubler les mots
20. Remplacer chiffre par lettre
21. Remplacer un mot par son image
22. Verlantiser les mots
23. Faire un anagramme aux mots
24. Remplacer certaines lettres par la lettre, qui se trouve à gauche, sur le clavier, d'elle
25. remplacer le mots par celui proposé par atome ( uniquement si il est different et même si c'est du code)
26. Ajouter 92 avant chaque 'i'.

## Règles de correction

1. Ajouter une voyelle/consonne
2. Remettre les mots dans le bon ordre.
3. Ajouter une nouvelle ponctuation.
4. créer des paragraphes de transition
5. Rendre le paragraphe cohérent avec l'ensemble des paragraphes
7. Rajouter 2 'i' lorsqu'on en voit un.
8. Choisir une voyelle et la supprimer à chaque fois qu'elle apparaît dans le paragraphes.
9. Supprimer les pronoms personnels (je, tu, il...).
10. Enlever deux lettres par mots.
11. Remplacer les mots par leurs contraires.
12. Traduire les mots anglais.
13. Inverser féminin et masculin.
14. Mettre en page (saut de ligne, titre...).
15. Mettre les chiffres en lettres si nécessaire.
16. Une même lettre par mots ( pas deux "A" dans un mot).



## Liste des participants

01. Ludivine
02. Gabin
03. Laura
04. Lorène
05. Ophélie
06. Thibaud
07. Cécile
08. Valentin
09. Calypso
10. Louis
11. Joséphine
12. Pierre
13. Jennifer
14. Louise
15. Théo
16. Soizic
17. Guillaume
18. Juliette
19. Louise
20. Johanna
21. Lise
22. Yasmina
23. Eléonore
24. Adel
